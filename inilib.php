<?php
/* ************************************************************************************************
 * LibINI
 * v1.0.0
 * 
 * Par Samayo Manati <https://gitlab.com/Winterskill>
 * 
 * LibINI est une petite librairie pour lire et écrire des données au format INI.
 *********************************************************************************************** */

namespace com\darksidegames\libini;

class IllegalINIContentException     extends \Exception {}
class UnknownINISectionNameException extends \Exception {}
class UnknownINIKeyNameException     extends \Exception {}
class WrongINISectionTypeException   extends \Exception {}
class IllegalCharacterException      extends \Exception {}

/**
 * Représente des données au format INI.
 * 
 * Les différentes sections INI peuvent être de plusieurs types :
 * (1) clef-valeur                           INIDocument::SECTION_TYPE_KEYVALUE
 *     Une section "normale", avec des paires constituées d'une clef et d'une
 *     valeur, séparés par un égal (=) ou un deux-points (:). Si il y a
 *     doublon, c'est la dernière instance de la clef qui prime.
 * (2) CSV                                        INIDocument::SECTION_TYPE_CSV
 *     Une section qui contient des données au format CSV, avec une virgule
 *     (,) comme séparateur entre champs, et un retour à la ligne comme
 *     séparateur entre entrées.
 * (3) valeur                                   INIDocument::SECTION_TYPE_VALUE
 *     Une section qui ne contient qu'une seule valeur, sans clef, et entourée
 *     de guillemets (""). Si il y a plusieurs valeurs données (sur plusieurs
 *     lignes), les différentes lignes seront concatennées (sans retour à la
 *     ligne automatique).
 * Les méthodes pour modifier / accéder au contenu des différentes sections
 * diffèrent selon le type de cette section. Par exemple, pour définir une clef
 * dans une section clef-valeur, on utilisera la méthode <code>setKey</code>,
 * tandis qu'on utilisera <code>setCSVKey</code> pour une section CSV et
 * <code>setValueKey</code> pour une section valeur.
 * 
 * Il est possible de consulter les expressions régulières utilisées au sein de
 * la classe via les constantes <code>REGEX_*</code>.
 * 
 * Les commentaires doivent débuter la ligne. Les caractères autorisés sont le
 * point-virgule (;) et le hashtag (#). Vous pouvez régler le caractère utilisé
 * dans la génération des données INI. Par défaut, c'est <code>';'</code>.
 * 
 * La classe INIDocument implémente également les méthodes magiques suivantes :
 * - __toString : retourne $this->render()
 * 
 * @author Samayo Manati
 */
class INIDocument {
	// =============== MEMBRES ===============
	protected $_beginComment = '';
	protected $_content = [];
	protected $_commentChar = ';';
	
	// =============== CONSTANTES PUBLIQUES ===============
	public const SECTION_TYPE_KEYVALUE = 0x1;
	public const SECTION_TYPE_CSV      = 0x2;
	public const SECTION_TYPE_VALUE    = 0x3;
	
	// =============== EXPRESSIONS REGULIERES ===============
	public const REGEX_AUTHORIZED_COMMENT_CHAR = '/^[;|#]$/';
	public const REGEX_COMMENT                 = '/^\s*[;|#](.*)$/';
	public const REGEX_SECTION_NAME            = '/^\s*\[(.+)\]\s*$/';
	public const REGEX_KEYVALUE                = '/^(.+)\s*[:=]\s*(.+)$/';
	public const REGEX_CSV                     = '/^,{0,1}(.+\s*,\s*)+.+$/';
	public const REGEX_CSV_SEPARATOR           = '/,/';
	public const REGEX_VALUE                   = '/^\s*"(.*)"\s*$/';

	// =============== METHODES MAGIQUES ===============
	/**
	 * @param string $datas Les données INI à mettre dans le document. Laissez
	 *                      vide pour générer un document INI vide.
	 * 
	 * @throws IllegalINIContentException Quand les donnees INI sont invalides.
	 */
	public function __construct(string $datas = '') {
		$this->parse($datas);
	}
	
	public function __toString(): string {
		return $this->render();
	}
	
	// =============== METHODES INTERNES ===============
	protected function parse(string $datas) {
		$linesDatas = preg_split('/\n/', $datas);
		$actualSection = '';
		// 1 = key = value
		// 2 = a , b , c
		// 3 = value
		$actualSectionContentType = self::SECTION_TYPE_KEYVALUE;
		$lastLineIsSectionName = false;
		$begin = true;
		
		for ($i = 0; $i < count($linesDatas); $i++) {
			$v = $linesDatas[$i];

			if (preg_match(self::REGEX_COMMENT, $v)) {
				// si c'est un commentaire
				if ($begin) {
					$a = [];
					preg_match(self::REGEX_COMMENT, $v, $a);
					$this->_beginComment .= $a[1] . "\n";
				}
			} elseif (preg_match(self::REGEX_SECTION_NAME, $v)) {
				// si c'est un nom de section
				$begin = false;
				
				$secName = [];
				preg_match(self::REGEX_SECTION_NAME, $v, $secName);
				$actualSection = $secName[1];
				$lastLineIsSectionName = true;
			} elseif (preg_match(self::REGEX_KEYVALUE, $v)) {
				// si c'est une paire clef-valeur
				$begin = false;
				
				if ($lastLineIsSectionName) {
					$lastLineIsSectionName = false;
					$actualSectionContentType = self::SECTION_TYPE_KEYVALUE;
				}
				
				if ($actualSectionContentType === self::SECTION_TYPE_KEYVALUE) {
					$line = [];
					preg_match(self::REGEX_KEYVALUE, $v, $line);
					$line[1] = preg_replace('/\s+$/', '', $line[1]); // split right
					$line[1] = preg_replace('/^\s+/', '', $line[1]); // split left
					$this->_content[$actualSection][$line[1]] = $line[2];
				} else {
					throw new IllegalINIContentException('must be of type key = val');
				}
			} elseif (preg_match(self::REGEX_CSV, $v)) {
				// si c'est une liste CSV
				$begin = false;
				
				if ($lastLineIsSectionName) {
					$lastLineIsSectionName = false;
					$actualSectionContentType = self::SECTION_TYPE_CSV;
				}
				
				if ($actualSectionContentType === self::SECTION_TYPE_CSV) {
					//$this->_content[$actualSection][] = split(';', $v);
					$this->_content[$actualSection][] = preg_split(self::REGEX_CSV_SEPARATOR, $v);
				} else {
					throw new IllegalINIContentException('must be of type a , b , c ...');
				}
			} elseif (preg_match(self::REGEX_VALUE, $v)) {
				// si c'est une valeur seule
				$begin = false;
				
				if ($lastLineIsSectionName) {
					$lastLineIsSectionName = false;
					$actualSectionContentType = self::SECTION_TYPE_VALUE;
				}
				
				if ($actualSectionContentType === self::SECTION_TYPE_VALUE) {
					$a = [];
					preg_match(self::REGEX_VALUE, $v, $a);
					if (isset($this->_content[$actualSection]))
						$this->_content[$actualSection] .= "\n" . $a[1];
					else
						$this->_content[$actualSection] = $a[1];
				} else
					throw new IllegalINIContentException('must be of type "value"');
			}
		}
	}

	// =============== METHODES PUBLIQUES ===============
	/**
	 * Retourne un tableau contenant les donnees d'une section.
	 * Si la section est de type valeur, un string et non un tableau sera
	 * retourné.
	 *
	 * @param string $sectionName Le nom de la section.
	 *
	 * @return array|string Le contenu de la section.
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 */
	public function getSection(string $sectionName) {
		if ($this->sectionExists($sectionName))
			return $this->_content[$sectionName];
		else
			throw new UnknownINISectionNameException($sectionName);
	}

	/**
	 * Retourne si une section existe.
	 *
	 * @param string $sectionName Le nom de la section.
	 *
	 * @return bool
	 */
	public function sectionExists(string $sectionName): bool {
		return isset($this->_content[$sectionName]);
	}

	/**
	 * Retourne le type de donnees d'une section.
	 *
	 * @param string $sectionName Le nom de la section.
	 *
	 * @return int
	 * 1 - clef-valeur
	 * 2 - CSV
	 * 3 - valeur
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 */
	public function getSectionType(string $sectionName): int {
		if ($this->sectionExists($sectionName)) {
			if (@is_array($this->_content[$sectionName][0])) // cette arobase est horrible mais blc
				return self::SECTION_TYPE_CSV;
			elseif (@is_string($this->_content[$sectionName])) // idem
				return self::SECTION_TYPE_VALUE;
			else
				return self::SECTION_TYPE_KEYVALUE;
		} else
			throw new UnknownINISectionNameException($sectionName);
	}

	/**
	 * Retourne une clef d'une section clef-valeur du document INI.
	 *
	 * @param string $sectionName Le nom de la section.
	 * @param string $keyName     Le nom de la clef.
	 * 
	 * @return string
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 * @throws UnknwonINIKeyNameException     Quand la clef n'existe pas.
	 * @throws WrongINISectionTypeException   Quand la section est de type CSV.
	 */
	public function getKey(string $sectionName, string $keyName = '') {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_KEYVALUE) {
				if ($this->keyExists($sectionName, $keyName))
					return $this->_content[$sectionName][$keyName];
				else
					throw new UnknownINIKeyNameException($keyName);
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Retourne une entrée d'une section CSV.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param int    $index       L'index de l'entrée à sélectionner.
	 * 
	 * @return array
	 * @throws UnknownINIKeyNameException Quand l'index est négatif ou qu'il
	 *                                    est plus grand que le nombre
	 *                                    d'entrées dans la section.
	 * @throws UnknownINISectionNameException Quand la section n'existe pas
	 * @throws WrongINISectionTypeException Quand la section n'est pas de
	 *                                      type CSV.
	 */
	public function getCSVKey(string $sectionName, int $index): array {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_CSV) {
				$sec = $this->getSection($sectionName);
				if ($index >= 0 && $index < count($sec))
					return $sec[$index];
				else
					throw new UnknownINIKeyNameException($index);
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Retourne si une clef existe dans une section clef-valeur.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param string $keyName     Le nom de la clef.
	 * 
	 * @return bool
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 * @throws WrongINISectionTypeException   Si la section n'est pas de
	 *                                        type clef-valeur.
	 */
	public function keyExists(string $sectionName, string $keyName): bool {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_KEYVALUE) {
				return isset($this->_content[$sectionName][$keyName]);
			} else
				// si c'est une section CSV ou valeur
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Crée une nouvelle section vide.
	 * 
	 * @param string $sectionName Le nom de la section. Ne doit pas déjà
	 *                            exister.
	 * @param int    $sectionType Le type de la section.
	 *                            1 - clef-valeur
	 *                            2 - CSV
	 *                            3 - valeur
	 * 
	 * @return void
	 */
	public function createSection(string $sectionName, int $sectionType = 1) {
		if ($this->sectionExists($sectionName))
			// si la section existe déjà
			return;
		else {
			if ($sectionType === self::SECTION_TYPE_KEYVALUE)
				$this->_content[$sectionName] = [];
			elseif ($sectionType === self::SECTION_TYPE_CSV)
				$this->_content[$sectionName] = [[]];
			elseif ($sectionType === self::SECTION_TYPE_VALUE)
				$this->_content[$sectionName] = '';
			return;
		}
	}
	
	/**
	 * Supprime une section.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * 
	 * @return void
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 */
	public function removeSection(string $sectionName) {
		if ($this->sectionExists($sectionName)) {
			$this->_content[$sectionName] = null;
			if (isset($this->_sectionsComments[$sectionName]))
				$this->_sectionsComments[$sectionName] = null;
			return;
		} else
			throw new UnknwonINISectionNameException($sectionName);
	}
	
	/**
	 * Supprime une clef contenue dans une section de type clef-valeur.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param string $keyName     Le nom de la clef.
	 * 
	 * @return void
	 * @throws UnknownINIKeyNameException     Quand la clef n'existe pas.
	 * @throws WrongINISectionTypeException   Quand la section est de type CSV.
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 */
	public function removeKey(string $sectionName, string $keyName) {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === 1) {
				if ($this->keyExists($sectionName, $keyName)) {
					$this->_content[$sectionName][$keyName] = null;
				} else
					throw new UnknownINIKeyNameException($keyName);
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Supprime un index d'une section CSV.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param int    $index       L'index à supprimer.
	 * 
	 * @return void
	 * @throws UnknownINIKeyNameException     Si l'index est négatif ou plus
	 *                                        grand que le nombre d'éléments
	 *                                        dans la section.
	 * @throws WrongINISectionTypeException   Si la section n'est pas de type
	 *                                        CSV.
	 * @throws UnknownINISectionNameException Si la section n'existe pas.
	 */
	public function removeCSVKey(string $sectionName, int $index) {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_CSV) {
				if ($index >= 0 && $index < count($this->_content[$sectionName]))
					$this->_content[$sectionName][$index] = null;
				else
					throw new UnknownINIKeyNameException($index);
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Vide une section.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * 
	 * @return void
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 */
	public function emptySection(string $sectionName) {
		if ($this->sectionExists($sectionName)) {
			$type = $this->getSectionType($sectionName);
			$this->removeSection($sectionName);
			$this->createSection($sectionName, $type);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Retourne le commentaire de début du document.
	 * 
	 * @return string
	 */
	public function getBeginComment(): string {
		return $this->_beginComment;
	}
	
	/**
	 * Définit le commentaire de début de document.
	 * 
	 * @param string $comment Le commentaire.
	 * 
	 * @return void
	 */
	public function setBeginComment(string $comment) {
		$this->_beginComment = $comment;
		return;
	}
	
	/**
	 * Retourne les données mises en formes.
	 * 
	 * @return string
	 */
	public function render(): string {
		$ret = '';
		
		if ($this->getBeginComment() !== "") {
			$commentByLines = preg_split('/\n/', $this->getBeginComment());
			foreach ($commentByLines as $i) {
				$ret .= $this->getCommentChar() . $i . "\n";
			}
			$ret .= "\n\n";
		}
		
		foreach ($this->_content as $k => $v) {
			// pour chaque section
			if ($k !== null && $k !== [] && $k !== [[]] && $k != '') {
				$ret .= '[' . $k . ']' . "\n";
				$sectionType = $this->getSectionType($k);
				if ($sectionType === self::SECTION_TYPE_KEYVALUE) {
					foreach ($v as $k2 => $v2)
						$ret .= $k2 . ' = ' . preg_replace('/\n/', "\\n", $v2) . "\n";
				} elseif ($sectionType === self::SECTION_TYPE_CSV) {
					foreach ($v as $v2) {
						$line = '';
						foreach ($v2 as $v3)
							$line .= preg_replace('/\n/', "\\n", $v3) . ',';
						$line = substr($line, 0, strlen($line) - 1);
						$ret .= $line . "\n";
					}
				} elseif ($sectionType === self::SECTION_TYPE_VALUE) {
					$splited = preg_split('/\n/', $v);
					foreach ($splited as $v2)
						$ret .= '"' . $v2 . '"' . "\n";
				}
				
				$ret .= "\n";
			}
		}
		
		return $ret;
	}
	
	/**
	 * Définit la valeur d'une clef dans une section clef-valeur.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param string $keyName     Le nom de la clef.
	 * @param string $keyValue    La valeur de la clef.
	 * 
	 * @return void
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 * @throws WrongINISectionTypeException   Si la section n'est pas de type
	 *                                        clef-valeur.
	 */
	public function setKey(string $sectionName, string $keyName, string $keyValue) {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_KEYVALUE) {
				$this->_content[$sectionName][$keyName] = $keyValue;
				return;
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Définit la valeur d'une entrée d'une section CSV.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param int    $index       L'index de l'entrée.
	 * @param array  $value       La valeur de l'entrée.
	 * 
	 * @return void
	 * @throws WrongINISectionTypeException   Quand la section n'est pas de
	 *                                        type CSV.
	 * @throws UnknownINISectionNameException Si la section n'existe pas.
	 */
	public function setCSVKey(string $sectionName, int $index, array $value) {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_CSV) {
				$this->_content[$sectionName][$index] = $value;
				return;
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Ajoute une entrée à la fin d'une section CSV.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param array  $value       La valeur de l'entrée.
	 * 
	 * @return void
	 * @throws UnknownINISectionNameException Si la section n'existe pas.
	 * @throws WrongINISectionTypeException   Si la section n'est pas de type
	 *                                        CSV.
	 */
	public function appendCSVKey(string $sectionName, array $value) {
		return $this->setCSVKey($sectionName, $this->getSectionSize($sectionName), $value);
	}
	
	/**
	 * Retourne la valeur d'une section valeur.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * 
	 * @return string La valeur
	 * @throws WrongINISectionTypeException   Quand la section n'est pas de
	 *                                        type valeur.
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 */
	public function getValueKey(string $sectionName): string {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_VALUE) {
				return $this->_content[$sectionName];
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Définit la valeur d'une section valeur.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * @param string $value       La valeur de la section.
	 * 
	 * @return void
	 * @throws UnknownINISectionNameException Quand la section n'existe pas.
	 * @throws WrongINISectionTypeException   Quand la section n'est pas de
	 *                                        type valeur.
	 */
	public function setValueKey(string $sectionName, string $value) {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_VALUE) {
				$this->_content[$sectionName] = $value;
				return;
			} else
				throw new WrongINISectionTypeException($sectionName);
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Retourne le caractère utilisé pour marquer un commentaire dans les
	 * données INI générées.
	 * 
	 * @return string Le caractère.
	 */
	public function getCommentChar(): string {
		return $this->_commentChar;
	}
	
	/**
	 * Définit le caractère utilisé pour marquer un commentaire dans les
	 * données INI générées.
	 * 
	 * @param string $char Le caractère.
	 * 
	 * @return void
	 * @throws IllegalCharacterException Si le caractère n'est pas valide par
	 *                                   rapport aux caractères autorisés.
	 */
	public function setCommentChar(string $char) {
		if (preg_match(self::REGEX_AUTHORIZED_COMMENT_CHAR, $char)) {
			$this->_commentChar = $char;
			return;
		} else
			throw new IllegalCharacterException($char);
	}
	
	/**
	 * Retourne la taille d'une section.
	 * 
	 * @param string $sectionName Le nom de la section.
	 * 
	 * @return int La taille.
	 * @throws UnknownINISectionNameException Si la section n'existe pas.
	 */
	public function getSectionSize(string $sectionName): int {
		if ($this->sectionExists($sectionName)) {
			if ($this->getSectionType($sectionName) === self::SECTION_TYPE_VALUE)
				return 1;
			else
				return count($this->getSection($sectionName));
		} else
			throw new UnknownINISectionNameException($sectionName);
	}
	
	/**
	 * Retourne le nombre de sections contenues dans le document INI.
	 * 
	 * @return int Le nombre.
	 */
	public function getSectionsNumber(): int {
		return count($this->_content);
	}
	
	/**
	 * Retourne le nombre de sections clef-valeur du document INI.
	 * 
	 * @return int Le nombre.
	 */
	public function getKeyValueSectionsNumber(): int {
		$c = 0;
		foreach ($this->_content as $k => $v) {
			if ($this->getSectionType($k) === self::SECTION_TYPE_KEYVALUE)
				$c++;
		}
		return $c;
	}
	
	/**
	 * Retourne le nombre de sections CSV du document INI.
	 * 
	 * @return int Le nombre.
	 */
	public function getCSVSectionsNumber(): int {
		$c = 0;
		foreach ($this->_content as $k => $v) {
			if ($this->getSectionType($k) === self::SECTION_TYPE_CSV)
				$c++;
		}
		return $c;
	}
	
	/**
	 * Retourne le nombre de sections valeur du document INI.
	 * 
	 * @return int Le nombre.
	 */
	public function getValueSectionsNumber(): int {
		$c = 0;
		foreach ($this->_content as $k => $v) {
			if ($this->getSectionType($k) === self::SECTION_TYPE_VALUE)
				$c++;
		}
		return $c;
	}
}

?>
